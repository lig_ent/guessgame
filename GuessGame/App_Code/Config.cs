﻿namespace GuessGame.App_Code
{
    internal static class Config
    {
        internal const int MinCountOfPlayers = 2;
        internal const int MaxCountOfPlayers = 8;

        internal const int MinBasketWeight = 40;
        internal const int MaxBasketWeight = 140;
        internal const int MaxCountOfAttempts = 100;
        internal const int MaxGameDuration = 1500; // ms
    }
}
