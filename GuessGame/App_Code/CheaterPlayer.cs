﻿using System;
using System.Collections.Concurrent;

namespace GuessGame.App_Code
{
    internal class CheaterPlayer : Player
    {
        private Random rnd;
        private ConcurrentDictionary<int, byte> memoryGlobal;

        internal CheaterPlayer(string name, ConcurrentDictionary<int, byte> memoryGlobal) : base(name)
        {
            this.memoryGlobal = memoryGlobal;
            this.rnd = RandomProvider.GetThreadRandom();
        }

        internal override int Guess()
        {
            int guess;

            do
            {
                guess = rnd.Next(Config.MinBasketWeight, Config.MaxBasketWeight + 1);
            } while (this.memoryGlobal.ContainsKey(guess));

            return guess;
        }
    }
}
