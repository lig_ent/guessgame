﻿using System.Collections.Concurrent;

namespace GuessGame.App_Code
{
    internal class ThoroughCheaterPlayer : Player
    {
        private int previousGuess;
        private ConcurrentDictionary<int, byte> memoryGlobal;

        internal ThoroughCheaterPlayer(string name, ConcurrentDictionary<int, byte> memoryGlobal) : base(name)
        {
            this.previousGuess = Config.MinBasketWeight;
            this.memoryGlobal = memoryGlobal;
        }

        internal override int Guess()
        {
            int guess = this.previousGuess++;

            while (this.memoryGlobal.ContainsKey(guess) && guess < Config.MaxBasketWeight)
            {
                guess++;
            };

            //this.memoryGlobal.Add(guess);

            return guess;
        }
    }
}
