﻿using System.Collections.Generic;
using System.Collections.Concurrent;

namespace GuessGame.App_Code
{
    internal class PlayerFactory
    {
        internal enum PlayerType
        {
            RandomPlayer,
            MemoryPlayer,
            ThoroughPlayer,
            CheaterPlayer,
            ThoroughCheaterPlayer
        };

        internal Player CreatePlayer(int playerType, string name, ref ConcurrentDictionary<int, byte> memoryGlobal)
        {
            Player player;
            
            switch ((PlayerType)playerType)
            {
                case PlayerType.RandomPlayer:
                    player = new RandomPlayer(name);
                    break;
                case PlayerType.MemoryPlayer:
                    player = new MemoryPlayer(name);
                    break;
                case PlayerType.ThoroughPlayer:
                    player = new ThoroughPlayer(name);
                    break;
                case PlayerType.CheaterPlayer:
                    player = new CheaterPlayer(name, memoryGlobal);
                    break;
                case PlayerType.ThoroughCheaterPlayer:
                    player = new ThoroughCheaterPlayer(name, memoryGlobal);
                    break;
                default:
                    player = new RandomPlayer(name);
                    break;
            }

            return player;
        }
    }
}
