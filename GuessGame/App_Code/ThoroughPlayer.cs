﻿using System;

namespace GuessGame.App_Code
{
    internal class ThoroughPlayer : Player
    {
        private int previousGuess;

        internal ThoroughPlayer(string name) : base(name)
        {
            this.previousGuess = Config.MinBasketWeight;
        }

        internal override int Guess()
        {
            int guess = this.previousGuess++;

            return guess;
        }
    }
}
