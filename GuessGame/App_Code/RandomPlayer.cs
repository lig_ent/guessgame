﻿using System;

namespace GuessGame.App_Code
{
    internal class RandomPlayer : Player
    {
        private Random rnd;

        internal RandomPlayer(string name) : base(name)
        {
            this.rnd = RandomProvider.GetThreadRandom();
        }

        internal override int Guess()
        {
            int guess = 0;

            guess = rnd.Next(Config.MinBasketWeight, Config.MaxBasketWeight + 1);            

            return guess;
        }
    }
}
