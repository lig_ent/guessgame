﻿using System;
using System.Collections.Generic;

namespace GuessGame.App_Code
{
    internal class MemoryPlayer : Player
    {
        private Random rnd;
        private List<int> memory;

        internal MemoryPlayer(string name) : base(name)
        {
            this.memory = new List<int>();
            this.rnd = RandomProvider.GetThreadRandom();
        }

        internal override int Guess()
        {
            int guess;

            do
            {
                guess = rnd.Next(Config.MinBasketWeight, Config.MaxBasketWeight + 1);
            } while (this.memory.Contains(guess));

            return guess;
        }

    }
}
