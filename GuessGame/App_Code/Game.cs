﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace GuessGame.App_Code
{
    internal class Game
    {
        private string result = "";

        private int basketWeight = 0;
        private long attemptsCount = 0;

        private long bestGuess = 0;
        private string bestGuessPlayerName = "";

        private bool IsGameEndedByWin = false;

        private readonly Object winGameLock = new object();

        private ConcurrentDictionary<int, byte> memoryGlobal;

        private CancellationTokenSource tokenSource;
        private CancellationToken token;

        private Random rnd;

        internal struct PlayerInfo
        {
            internal string Name;
            internal int PlayerType;

            internal PlayerInfo(string name, int playerType)
            {
                this.Name = name;
                this.PlayerType = playerType;
            }
        }

        internal Game()
        {
            this.rnd = RandomProvider.GetThreadRandom();
            this.memoryGlobal = new ConcurrentDictionary<int, byte>();

            this.tokenSource = new CancellationTokenSource();
            this.token = tokenSource.Token;
        }

        internal string Start(List<PlayerInfo> playersInfo)
        {
            List<Player> players = new List<Player>();

            PlayerFactory playerFactory = new PlayerFactory();
            foreach (PlayerInfo playerInfo in playersInfo)
            {
                players.Add(playerFactory.CreatePlayer(playerInfo.PlayerType, playerInfo.Name, ref memoryGlobal));
            }            

            basketWeight = rnd.Next(Config.MinBasketWeight, Config.MaxBasketWeight + 1);

            result += String.Format("Basket weight: {0}.\n", basketWeight);

            List<Task> tasks = new List<Task>();

            foreach (Player player in players)
            {
                tasks.Add(new Task(() => PlayGame(player), token));
            }

            try
            {
                Parallel.ForEach(tasks, task => task.Start());
                Task.WaitAll(tasks.ToArray(), Config.MaxGameDuration);
            }
            catch (AggregateException exceptions)
            {
                foreach (var ex in exceptions.InnerExceptions)
                {
                    if (!(ex is TaskCanceledException))
                    {
                        result += String.Format("xception: {0}\n", ex.GetType().Name);
                    }
                }
            }
            finally
            {
                tokenSource.Dispose();
            }

            if (IsGameEndedByWin)
            {
                result += String.Format("Player '{0}' won. Total attempts in the game: {1}.\n", bestGuessPlayerName, attemptsCount);
            }
            else
            {
                result += String.Format("The best guess: {0}. Done by '{1}'.\n", bestGuess, bestGuessPlayerName);
            }

            result += String.Format("Game is finished.\n");

            return result;
        }
        
        private void PlayGame(Player player)
        {
            int guess;

            for (int i = 0; i < Config.MaxCountOfAttempts; i++)
            {
                if (IsGameEnded())
                {
                    token.ThrowIfCancellationRequested();
                    break;
                }

                Interlocked.Increment(ref attemptsCount);

                guess = player.Guess();

                memoryGlobal.TryAdd(guess, 0);

                if (guess == basketWeight)
                {
                    WinGame(player.Name);
                    break;
                }
                else
                {
                    int delta = (guess > basketWeight) ? guess - basketWeight : basketWeight - guess;
                    LookForBestGuess(guess, player.Name);
                    Thread.Sleep(delta);
                }
            }
        }
        
        private void LookForBestGuess(int newGuess, string playerName)
        {
            int newDelta = (newGuess > basketWeight) ? newGuess - basketWeight : basketWeight - newGuess;
            long bestDelta = (Interlocked.Read(ref bestGuess) > basketWeight) ? Interlocked.Read(ref bestGuess) - basketWeight : basketWeight - Interlocked.Read(ref bestGuess);

            if (newDelta < bestDelta)
            {
                Interlocked.Exchange(ref bestGuess, newGuess);
                Interlocked.Exchange(ref bestGuessPlayerName, playerName);
            }
        }

        private bool IsGameEnded()
        {
            if (token.IsCancellationRequested)
            {
                return true;
            }

            if (Interlocked.Read(ref attemptsCount) >= Config.MaxCountOfAttempts)
            {
                return true;
            }

            return false;
        }

        private void WinGame(string name)
        {
            lock (winGameLock)
            {
                if (!token.IsCancellationRequested)
                {
                    tokenSource.Cancel();

                    Interlocked.Exchange(ref bestGuessPlayerName, name);

                    IsGameEndedByWin = true;
                }
            }
        }
    }
}
