﻿using System;

namespace GuessGame.App_Code
{
    internal abstract class Player
    {
        internal string Name;

        internal abstract int Guess();

        internal Player(string name)
        {
            this.Name = name;
        }
    }
}
