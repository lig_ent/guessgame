﻿using GuessGame.App_Code;
using System;
using System.Collections.Generic;

namespace GuessGame
{
    public class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();

            List<Game.PlayerInfo> players = InputData();

            string result = game.Start(players);

            Console.WriteLine(result);
        }

        private static List<Game.PlayerInfo> InputData()
        {
            string input;
            int countOfPlayers = Config.MinCountOfPlayers;
            List<Game.PlayerInfo> playersInfo = new List<Game.PlayerInfo>();

            do
            {
                Console.WriteLine("Input number of players from {0} to {1}: ", Config.MinCountOfPlayers, Config.MaxCountOfPlayers);
                input = Console.ReadLine();
            } while (!Int32.TryParse(input, out countOfPlayers));

            if (countOfPlayers < Config.MinCountOfPlayers) countOfPlayers = Config.MinCountOfPlayers;
            if (countOfPlayers > Config.MaxCountOfPlayers) countOfPlayers = Config.MaxCountOfPlayers;

            for (int i = 0; i < countOfPlayers; i++)
            {
                Console.WriteLine("Input name of '{0}' user: ", i);
                string inputName = Console.ReadLine();

                int inputType = 0;

                do
                {
                    Console.WriteLine("Input type of '{0}' user (from 0 to 4): ", inputName);
                    input = Console.ReadLine();
                } while (!Int32.TryParse(input, out inputType));

                if (inputType < 0) inputType = 0;
                if (inputType > 4) inputType = 4;

                playersInfo.Add(new Game.PlayerInfo(inputName, inputType));
            }

            Console.WriteLine();

            return playersInfo;
        }
    }
}
